extern crate pegorust;
extern crate regex;

use pegorust::*;
use regex::Regex;

#[cfg(test)]
#[test]
fn test_str_parser() {
    let abcdef = StrParser("abcdef");
    assert!(abcdef.try_match("abcdefffff").is_match());
    assert!(abcdef.try_match("abcfdefffff").is_fail());
    assert!(abcdef.try_match("abcf").is_fail());
    assert!(abcdef.try_match("").is_fail());
}

#[cfg(test)]
#[test]
fn test_char_parser() {
    let a = CharParser('a');
    assert!(a.try_match("baaaaa").is_fail());
    assert!(a.try_match("aaaaa").is_match());
}

#[cfg(test)]
#[test]
fn test_regex_parser() {
    let email = RegexParser(Regex::new("^[a-z_A-Z][a-z_A-Z0-9]*@[a-z]+\\.(com|org|info)").unwrap());
    let m = email.try_match(&"someName84@example.com[some string]");
    assert!(m.is_match());
    match m {
        MatchResult::Success(s, cursor) => {
            assert!(s == "someName84@example.com");
            assert!(cursor.slice_after() == "[some string]");
        }
        _ => {}
    }
    assert!(email.try_match("[someName84@example.com]").is_fail());
    assert!(email.try_match("     someName84@example.com").is_fail());
}

#[cfg(test)]
#[test]
fn test_then_parser() {
    let abcc = RegexParser(Regex::new("^abc+").unwrap());
    let ddef = RegexParser(Regex::new("^d+ef").unwrap());
    let parser = ThenParser(&abcc, &ddef);
    let m = parser.try_match("abcccccdddddefggg");
    assert!(m.is_match());
    match m {
        MatchResult::Success(s, cursor) => {
            assert!(s == "dddddef");
            assert!(cursor.slice_after() == "ggg");
        }
        _ => {}
    }
}

#[cfg(test)]
#[test]
fn test_before_parser() {
    let abcc = RegexParser(Regex::new("^abc+").unwrap());
    let ddef = RegexParser(Regex::new("^d+ef").unwrap());
    let parser = BeforeParser(&abcc, &ddef);
    let m = parser.try_match("abcccccdddddefggg");
    assert!(m.is_match());
    match m {
        MatchResult::Success(s, cursor) => {
            assert!(s == "abccccc");
            assert!(cursor.slice_after() == "ggg");
        }
        _ => {}
    }
}

#[cfg(test)]
#[test]
fn test_then_before_parsers() {
    let abcc = RegexParser(Regex::new("^abc+").unwrap());
    let ddef = RegexParser(Regex::new("^d+ef").unwrap());
    let ghhi = RegexParser(Regex::new("^gh+i").unwrap());
    let then_parser = ThenParser(&abcc, &ddef);
    let parser = BeforeParser(&then_parser, &ghhi);
    let m = parser.try_match("abcccccdddddefghhhhhhikkkkkkk");
    assert!(m.is_match());
    match m {
        MatchResult::Success(s, cursor) => {
            assert!(s == "dddddef");
            assert!(cursor.slice_after() == "kkkkkkk");
        }
        _ => panic!()
    }
}

#[cfg(test)]
#[test]
fn test_optional_parser() {
    let abcc = RegexParser(Regex::new("^abc+").unwrap());
    let parser = OptionalParser(&abcc);
    {
        let m = parser.try_match("abcccccdddddefghhhhhhikkkkkkk");
        assert!(m.is_match());
        match m {
            MatchResult::Success(s, cursor) => {
                assert_eq!(s, Option::Some("abccccc"));
                assert_eq!(cursor.slice_after(), "dddddefghhhhhhikkkkkkk");
            }
            _ => panic!()
        }
    }
    {
        let m = parser.try_match("abfcccccdddddefghhhhhhikkkkkkk");
        assert!(m.is_match());
        match m {
            MatchResult::Success(s, cursor) => {
                assert_eq!(s, Option::None);
                assert_eq!(cursor.slice_after(), "abfcccccdddddefghhhhhhikkkkkkk");
            }
            _ => panic!()
        }
    }
}
