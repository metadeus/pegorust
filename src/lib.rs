extern crate regex;
extern crate strcursor;

use strcursor::StrCursor;
use std::cmp::min;
use regex::Regex;

pub enum MatchResult<'a, T> {
    Success(T, StrCursor<'a>),
    Fail
}

impl<'a, T> MatchResult<'a, T> {
    pub fn is_match(&self) -> bool {
        match *self {
            MatchResult::Success(_, _) => true,
            _ => false
        }
    }
    pub fn is_fail(&self) -> bool {
        match *self {
            MatchResult::Success(_, _) => false,
            _ => true
        }
    }
}

fn match_result_of<'a>(condition: bool, cursor: StrCursor<'a>) -> MatchResult<'a, ()> {
    match condition {
        true => MatchResult::Success((), cursor),
        _ => MatchResult::Fail
    }
}

pub trait Parser<'a, T> {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, T>;
    fn try_match(&self, input: &'a str) -> MatchResult<'a, T> {
        self.try_match_cursor(&mut StrCursor::new_at_start(input))
    }
}

pub struct CharParser(pub char);

impl<'a> Parser<'a, ()> for CharParser {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, ()> {
        match input.cp_after() {
            Some(c) => match_result_of(c == self.0, input.at_next().unwrap()),
            _ => MatchResult::Fail
        }
    }
}

pub struct StrParser<'a>(pub &'a str);

impl<'a> Parser<'a, ()> for StrParser<'a> {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, ()> {
        let end_cursor = StrCursor::new_at_cp_right_of_byte_pos(input.slice_after(),
                                                                min(input.byte_pos() + self.0.len(),
                                                                    StrCursor::new_at_end(input.slice_all()).byte_pos()));
        match_result_of(self.0 == input.slice_between(end_cursor).unwrap(), end_cursor)
    }
}

pub struct RegexParser(pub Regex);

impl<'a> Parser<'a, &'a str> for RegexParser {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, &'a str> {
        match self.0.find(input.slice_after()) {
            Some((from, to)) => {
                assert!(from == 0, "invalid regex, must match from the start");
                let end_cursor = StrCursor::new_at_cp_right_of_byte_pos(input.slice_all(), input.byte_pos() + to - from);
                MatchResult::Success(input.slice_between(end_cursor).unwrap(), end_cursor)
            }
            _ => MatchResult::Fail
        }
    }
}

pub struct ThenParser<'a, T1: 'a, T2: 'a>(pub &'a Parser<'a, T1>, pub &'a Parser<'a, T2>);

impl<'a, T1: 'a, T2: 'a> Parser<'a, T2> for ThenParser<'a, T1, T2> {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, T2> {
        match self.0.try_match_cursor(input) {
            MatchResult::Success(_, mut c) => self.1.try_match_cursor(&mut c),
            _ => MatchResult::Fail
        }
    }
}

pub struct BeforeParser<'a, T1: 'a, T2: 'a>(pub &'a Parser<'a, T1>, pub &'a Parser<'a, T2>);

impl<'a, T1: 'a, T2: 'a> Parser<'a, T1> for BeforeParser<'a, T1, T2> {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, T1> {
        match self.0.try_match_cursor(input) {
            MatchResult::Success(val, mut c) => {
                match self.1.try_match_cursor(&mut c) {
                    MatchResult::Success(_, c2) => MatchResult::Success(val, c2),
                    _ => MatchResult::Fail
                }
            },
            _ => MatchResult::Fail
        }
    }
}

pub struct OptionalParser<'a, T: 'a>(pub &'a Parser<'a, T>);

impl<'a, T: 'a> Parser<'a, Option<T>> for OptionalParser<'a, T> {
    fn try_match_cursor(&self, input: &mut StrCursor<'a>) -> MatchResult<'a, Option<T>> {
        match self.0.try_match_cursor(input) {
            MatchResult::Success(v, c) => MatchResult::Success(Option::Some(v), c),
            _ => MatchResult::Success(Option::None, *input)
        }
    }
}